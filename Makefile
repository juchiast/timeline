make:
	cargo build --release
install:
	install ./target/release/timeline /usr/bin
	install timeline.service /usr/lib/systemd/system
