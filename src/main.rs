use std::fmt;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::mem;
use std::process::Command;

struct Duration {
    t: i64,
}
impl Duration {
    fn new(t: i64) -> Self {
        Duration { t }
    }
}
impl fmt::Display for Duration {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt,
            "{}h{}m{}s",
            self.t / 3600,
            self.t / 60 % 60,
            self.t % 60
        )
    }
}

#[derive(Debug, Clone)]
enum Range {
    All,
    FromEnd(u64),
    FromStart(u64),
    FromDay(Day, u64),
    BetweenDay(Day, Day),
}

#[derive(Debug, Clone, Ord, PartialOrd, PartialEq, Eq)]
struct Day {
    pub year: i64,
    pub month: i64,
    pub day: i64,
}

impl fmt::Display for Day {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:2}-{:2}-{:4}", self.day, self.month, self.year)
    }
}

#[derive(Debug, Clone)]
struct Time {
    pub hour: i64,
    pub minute: i64,
    pub second: i64,
}

#[derive(Debug, Clone)]
struct Date {
    pub e: i64,
    pub day: Day,
    pub time: Time,
}
impl fmt::Display for Date {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt,
            "{:010}{:04}{:02}{:02}{:02}{:02}{:02}",
            self.e,
            self.day.year,
            self.day.month,
            self.day.day,
            self.time.hour,
            self.time.minute,
            self.time.second
        )
    }
}
impl Date {
    fn from_vec(vec: Vec<u8>) -> Option<Date> {
        let mut rec: [i64; 7] = [0; 7];
        let size: [usize; 7] = [10, 4, 2, 2, 2, 2, 2];
        let mut iter = vec.into_iter();
        for i in 0..7 {
            for _ in 0..size[i] {
                let c = iter.next()?;
                rec[i] = rec[i] * 10 + (c as i64 - 48);
            }
        }
        Some(unsafe { mem::transmute(rec) })
    }
}

#[derive(Debug, Clone)]
enum Data {
    Half(Date),
    Full(Date, Date),
}
impl Data {
    fn get_day(&self) -> Day {
        match *self {
            Data::Half(ref x) | Data::Full(ref x, _) => x,
        }.day
            .clone()
    }
    fn get_days(&self) -> (Day, Day) {
        match *self {
            Data::Half(_) => panic!("get_days() on a half data"),
            Data::Full(ref x, ref y) => (x.day.clone(), y.day.clone()),
        }
    }
    fn get_date(&self) -> Date {
        match *self {
            Data::Half(ref x) | Data::Full(ref x, _) => x,
        }.clone()
    }
    fn get_dates(&self) -> (Date, Date) {
        match *self {
            Data::Half(_) => panic!("get_dates() on a half data"),
            Data::Full(ref x, ref y) => (x.clone(), y.clone()),
        }
    }
}

fn current_time() -> Date {
    let out = Command::new("date")
        .arg("+%s%Y%m%d%H%M%S")
        .output()
        .expect("`date` error");
    // TODO error message
    Date::from_vec(out.stdout).unwrap()
}
fn timezone() -> i64 {
    let out = Command::new("date").arg("+%z").output().unwrap().stdout;
    let mut out = String::from_utf8(out).unwrap();
    out.pop();
    out.parse::<i64>().unwrap()
}
fn read(mut file: File) -> Vec<Data> {
    let mut ret: Vec<Data> = Vec::new();
    let mut buf: Vec<u8> = Vec::new();
    file.read_to_end(&mut buf).expect("file read error");
    let len = buf.len();
    if len % 24 != 0 {
        panic!("data file error");
    }
    let iter = buf.into_iter();
    let mut tmp: Vec<Date> = Vec::new();
    for i in 0..(len / 24) {
        tmp.push(Date::from_vec(iter.clone().skip(24 * i).take(24).collect()).unwrap());
    }
    let len = tmp.len();
    let mut iter = tmp.into_iter();
    for i in 0..(len / 2) {
        ret.push(Data::Full(iter.next().unwrap(), iter.next().unwrap()));
        if len % 2 == 1 && i == len / 2 - 1 {
            ret.push(Data::Half(iter.next().unwrap()));
        }
    }
    ret
}

fn read_range(data: Vec<Data>, range: Range) -> Vec<Data> {
    let len = data.len();
    match range {
        Range::All => data,
        Range::FromStart(n) => data.into_iter().take(n as usize).collect(),
        Range::FromEnd(n) => {
            let n = n as usize;
            data.into_iter()
                .skip(if len > n { len - n } else { 0 })
                .collect()
        }
        Range::FromDay(day, n) => data.into_iter()
            .skip_while(|x| x.get_day() < day)
            .take(n as usize)
            .collect(),
        Range::BetweenDay(d1, d2) => data.into_iter()
            .skip_while(|x| x.get_day() < d1)
            .take_while(|x| x.get_day() <= d2)
            .collect(),
    }
}

fn read_range_d(data: Vec<Data>, range: Range) -> Vec<Data> {
    let zone = timezone() * 36;
    let get = |data: &Data| (data.get_date().e + zone) / 86400 * 86400;
    match range {
        Range::All => data,
        Range::FromStart(n) => {
            let e = get(&data[0]) + (n as i64 - 1) * 86400;
            data.into_iter().take_while(|x| get(x) <= e).collect()
        }
        Range::FromEnd(n) => {
            let e = get(data.last().unwrap()) - (n as i64 - 1) * 86400;
            data.into_iter().skip_while(|x| get(x) < e).collect()
        }
        Range::FromDay(day, n) => {
            let e = {
                let x = match data.iter().skip_while(|x| x.get_day() < day).next() {
                    None => return Vec::new(),
                    Some(x) => x,
                };
                get(x) + (n as i64 - 1) * 86400
            };
            data.into_iter()
                .skip_while(|x| x.get_day() < day)
                .take_while(|x| get(x) <= e)
                .collect()
        }
        Range::BetweenDay(d1, d2) => data.into_iter()
            .skip_while(|x| x.get_day() < d1)
            .take_while(|x| x.get_day() <= d2)
            .collect(),
    }
}

fn shift_day(mut data: Vec<Data>) -> Vec<Data> {
    if let Data::Half(_) = data[data.len() - 1] {
        let x = data.pop().unwrap().get_date();
        data.push(Data::Full(x, current_time()));
    }

    let mut tmp: Vec<Data> = Vec::new();
    let (mut last, mut off) = data[0].get_dates();
    for item in data.iter().skip(1) {
        let (curr, off_time) = item.get_dates();
        if curr.day == last.day {
            off.e += off_time.e - curr.e;
        } else {
            tmp.push(Data::Full(last, off));
            last = curr;
            off = off_time;
        }
    }
    tmp.push(Data::Full(last, off));

    tmp
}

fn print_help() {}

fn on(mut file: File) {
    let len = file.metadata().expect("metadata failed").len();
    if len % 48 == 24 {
        return println!("Please turn off before turn on");
    } else if len % 48 != 0 {
        panic!("data file error");
    }
    write!(file, "{}", current_time()).expect("cannot write");
}
fn off(mut file: File) {
    let len = file.metadata().expect("metadata failed").len();
    if len % 48 == 0 {
        return println!("Please turn on before turn off");
    } else if len % 48 != 24 {
        panic!("data file error");
    }
    write!(file, "{}", current_time()).expect("cannot write");
}
fn now(data: Vec<Data>) {
    if data.is_empty() {
        return ();
    }
    let last = data.into_iter().last();
    if let Some(Data::Half(x)) = last {
        println!("{}", Duration::new(current_time().e - x.e));
    } else {
        println!("please turn on");
    }
}
fn cat(data: Vec<Data>, range: Range, graph: bool) {
    if data.is_empty() {
        return ();
    }
    let data: Vec<Data> = read_range(data, range);

    if graph {
        let gfmt = |a: &Date, b: &Date| -> String {
            let calc = |a: &Time| a.hour * 2 + a.minute / 30;
            let s = calc(&a.time);
            let f = calc(&b.time);
            let mut fmt = String::new();
            for x in 0..48 {
                fmt += if ((s <= f) && (s <= x) && (x <= f)) || ((s > f) && ((x <= f) || (x >= s)))
                {
                    "*"
                } else {
                    " "
                };
            }
            let stat = format!(
                "{:02}:{:02}: {}",
                a.time.hour,
                a.time.minute,
                Duration::new(b.e - a.e)
            );

            fmt + " " + &stat
        };

        let mut prev = String::new();
        for x in data {
            let (a, b) = match x {
                Data::Full(a, b) => (a, b),
                Data::Half(a) => (a, current_time()),
            };
            let mut current = a.day.to_string();
            if prev == current {
                current = "          ".to_owned();
            } else {
                prev = current.clone();
            }
            println!("{}: {}", current, gfmt(&a, &b));
        }
    } else {
        let fmt = |a: &Date| -> String {
            format!(
                "{:02}-{:02}-{:04} {:02}:{:02}:{:02}",
                a.day.day, a.day.month, a.day.year, a.time.hour, a.time.minute, a.time.second
            )
        };
        for x in data {
            match x {
                Data::Full(a, b) => {
                    println!("{} {} {}", fmt(&a), fmt(&b), Duration::new(b.e - a.e));
                }
                Data::Half(a) => println!("{}", fmt(&a)),
            }
        }
    }
}
fn sum(data: Vec<Data>, range: Range, unit: u64, graph: bool) {
    if data.is_empty() {
        return ();
    }
    let data = read_range(shift_day(data), range);
    let zone = timezone() * 36;
    let z = if let Data::Full(ref x, _) = data[0] {
        (x.e + zone) / 86400 * 86400
    } else {
        panic!("error")
    };
    let data: Vec<(Date, i64)> = data.into_iter().fold(vec![], |mut vec, item| {
        let (mut d1, d2) = item.get_dates();
        let t = d2.e - d1.e;
        d1.e = (d1.e + zone - z) / (86400 * unit as i64);
        let len = vec.len();
        if vec.is_empty() || d1.e != vec[len - 1].0.e {
            vec.push((d1, t))
        } else {
            vec[len - 1].1 += t;
        }
        vec
    });
    let max = data.iter().map(|x| x.1).max().unwrap();
    let each = (max / 55 / 900 + 1) * 900;
    let fmt = |t: i64| -> String {
        if graph {
            "*".repeat((t / each) as usize)
        } else {
            Duration::new(t).to_string()
        }
    };
    let mut sum = 0;
    for (date, t) in data {
        println!("{}: {}", date.day, fmt(t));
        sum += t;
    }
    if graph {
        print!("Each dot is {}. ", Duration::new(each));
    }
    println!("Total {}", Duration::new(sum));
}

fn stat(data: Vec<Data>, range: Range) {
    if data.is_empty() {
        return ();
    }
    let data = read_range_d(data, range);
    let mut graph: [i64; 24] = [0; 24];
    let zone = timezone() * 36;
    let add = |g: &mut [i64; 24], s: &Date, f: &Date| {
        let s = s.e + zone;
        let f = f.e + zone;
        let sh = s as usize / 3600;
        let fh = f as usize / 3600;
        for x in sh..fh {
            g[x % 24] += 3600;
        }
        g[sh % 24] -= s % 3600;
        g[fh % 24] += f % 3600;
    };
    for x in &data {
        match *x {
            Data::Full(ref s, ref f) => add(&mut graph, s, f),
            Data::Half(ref s) => add(&mut graph, s, &current_time()),
        }
    }
    let sum = graph.iter().sum();
    let max = graph.iter().max().unwrap();
    let each = (max / 55 / 600 + 1) * 600;
    let fmt = |t: i64| -> String {
        let ret = "*".repeat((t / each) as usize);
        format!("{} {}", ret, t / each)
    };
    for (i, x) in graph.iter().enumerate() {
        println!("{:2}: {}", i, fmt(*x));
    }
    println!(
        "Each dot is {}. Total {}.",
        Duration::new(each),
        Duration::new(sum)
    );
}

fn main() {
    let data_file: &'static str = "/var/db/timeline";
    let args: Vec<String> = std::env::args()
        .skip(1)
        .map(|x| if x == "-g" { "--graph".to_owned() } else { x })
        .collect();
    let mutfile = || {
        std::fs::OpenOptions::new()
            .create(true)
            .append(true)
            .open(data_file)
            .expect("cannot open file")
    };
    let readfile = || {
        std::fs::OpenOptions::new()
            .read(true)
            .open(data_file)
            .expect("cannot open file")
    };
    let day_from_string = |s: &String| -> Option<Day> {
        let vec: Vec<u8> = s.clone()
            .into_bytes()
            .into_iter()
            .filter(|&x| (x >= 48) && (x <= 57))
            .collect();
        let s = unsafe { String::from_utf8_unchecked(vec) };
        let s = format!("0000000000{}000000", s);
        Date::from_vec(s.into_bytes()).map(|x| x.day)
    };
    let get_range = |vec: &Vec<String>| -> Option<Range> {
        if vec.is_empty() {
            return None;
        }
        let num = vec[0].parse::<u64>();
        if num.is_ok() {
            return Some(Range::FromEnd(num.unwrap()));
        }
        if vec[0] == "--all" {
            return Some(Range::All);
        } else if vec[0] == "-s" {
            if vec.len() < 2 {
                return None;
            }
            let num = vec[1].parse::<u64>();
            if num.is_ok() {
                return Some(Range::FromStart(num.unwrap()));
            } else {
                return None;
            }
        } else if vec[0] == "-fd" {
            if vec.len() < 3 {
                return None;
            }
            let day = day_from_string(&vec[1]);
            let num = vec[2].parse::<u64>();
            if day.is_none() || num.is_err() {
                return None;
            } else {
                return Some(Range::FromDay(day.unwrap(), num.unwrap()));
            }
        } else if vec[0] == "-bd" {
            if vec.len() < 3 {
                return None;
            }
            let (day1, day2) = (day_from_string(&vec[1]), day_from_string(&vec[2]));
            if day1.is_none() || day2.is_none() {
                return None;
            } else {
                return Some(Range::BetweenDay(day1.unwrap(), day2.unwrap()));
            }
        }
        Some(Range::FromEnd(0))
    };
    let get_unit = |vec: &Vec<String>| -> Option<u64> {
        for i in 0..vec.len() {
            if vec[i] == "-u" && i + 1 < vec.len() {
                let num = vec[i + 1].parse::<u64>();
                if num.is_ok() {
                    return Some(num.unwrap());
                } else {
                    return None;
                }
            }
        }
        None
    };
    if args.is_empty() {
        return print_help();
    }

    if args[0] == "on" {
        on(mutfile());
    } else if args[0] == "off" {
        off(mutfile());
    } else if args[0] == "now" {
        now(read(readfile()));
    } else if args[0] == "cat" {
        let args: Vec<String> = args.into_iter().skip(1).collect();
        let range = get_range(&args);
        let graph = args.iter().any(|x| x == "--graph");
        if range.is_none() {
            return print_help();
        }
        cat(read(readfile()), range.unwrap(), graph);
    } else if args[0] == "sum" {
        let args: Vec<String> = args.into_iter().skip(1).collect();
        let range = get_range(&args);
        if range.is_none() {
            return print_help();
        }
        let unit = get_unit(&args).unwrap_or(1);
        let graph = args.iter().any(|x| x == "--graph");
        sum(read(readfile()), range.unwrap(), unit, graph);
    } else if args[0] == "stat" {
        let args: Vec<String> = args.into_iter().skip(1).collect();
        let range = get_range(&args);
        stat(read(readfile()), range.unwrap_or(Range::All));
    }
}
